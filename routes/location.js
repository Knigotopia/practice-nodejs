const jwt = require("jsonwebtoken");
const express = require('express');
const router = express.Router();
const models = require('../models');
const app = require('../app');
const middlewares = require('../middleware');

router.post('/', (req, res) => {
    const {UserId, name, type, lat, lng} = req.body;
    models.Location
        .create({UserId, name, type, lat, lng})
        .then(location => {
          res.json(location)
        })
        .catch(err => console.log(err));
});

router.get('/', (req, res) => {
    models.Location
        .findAll()
        .then(getLocation =>
          res.json(getLocation)
        )
        .catch(err => console.log(err));
});

router.put('/:id', middlewares, (req, res) => {
    const {name} = req.body;
    const { id } =  req.params;
    models.Location.update({ name }, { where: { id } })
    .then(() => {
      res.send({ token:  jwt.sign({name}, 'superSecret', { expiresIn: "24h" }) }); // 1
    })
    .catch(err => console.log(err));
});

router.delete('/:id', (req, res) => {
    const { id } =  req.params;
    models.Location.destroy({ where: { id } })
    .then((data) => {
      res.send({deleted: data});
    })
    .catch(err => console.log(err));
});


module.exports = router;
