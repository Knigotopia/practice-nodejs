const models = require('../models');
const express = require('express');
const middlewares = require('../middleware');
const router = express.Router();

router.get('/:id', (req, res) => {
    models.User
        .findOne({id: req.params.id})
        .then(user => res.json(user))
        .catch(err => res.status(404).send('User not found.'));
});

router.get('/', (req, res) => {
    models.User
        .findAll()
        .then(users => res.json(users))
        .catch(err => res.status(404).send('Users not found.'));
});

router.put('/:id', middlewares, (req, res) => {
    const {name, password, age, occupation} = req.body;
    const { id } =  req.params;
    models.User.update({ name, password, age, occupation }, { where: { id } })
    .then(() => {
      res.send({ token:  jwt.sign({name}, 'superSecret', { expiresIn: "24h" }) }); // 1
    })
    .catch(err => console.log(err));
});

router.delete('/:id', (req, res) => {
    const { id } =  req.params;
    models.User.destroy({ where: { id } })
    .then((data) => {
      res.send({deleted: data});
    })
    .catch(err => console.log(err));
});

router.post('/', (req, res) => {
    const {name, password, age, occupation} = req.body;
    models.User
        .create({name, password, age, occupation})
        .then(create => {
            res.json(create)
        })
        .catch(err => console.log(err));
});

module.exports = router;
