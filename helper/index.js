const jwt = require("jsonwebtoken");
const app = require('../app');

const token = function(name){
  console.log('name', name)
  const jwt = jwt.sign({name}, app.get('superSecret'), { // create a token
    expiresIn: "24h" // expires in 24 hours
  })
  console.log(jwt);
  return jwt;
}

module.exports = token;
